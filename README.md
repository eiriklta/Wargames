# Running the program

To run the program you have to install it by zip, or clone it with: git clone git@gitlab.stud.idi.ntnu.no:eiriklta/Wargames.git

Then you enter cd Wargames in the command prompt.

Finally run the command: mvn clean javafx:run, to launch the program.

