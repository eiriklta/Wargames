package Game.War.Units;

import Game.War.Units.SpecialUnits.CavalryUnit;
import Game.War.Units.SpecialUnits.CommanderUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Units.SpecialUnits.RangedUnit;
import Game.War.Army;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ArmyTest {

    Army army;

    @Test
    public void constructor_throws_exception_for_blank_name() {
        assertThrows(IllegalArgumentException.class, () -> {
            army = new Army("");
        });
    }

    @Test
    public void getting_random_unit_throws_exception_when_empty() {
        army = new Army("Name");
        assertThrows(IllegalArgumentException.class, () -> army.getRandom());
    }

    @Test
    public void test_for_getting_specialized_units() {

        ArrayList<Unit> units = new ArrayList<Unit>();

        for(int i = 0; i < 10; i++) {
            units.add(new InfantryUnit("Infantry", 10));
            units.add(new RangedUnit("Ranged", 10));
            units.add(new CavalryUnit("Cavalry", 10));
            units.add(new CommanderUnit("Commander", 10));
        }

        army = new Army("TestArmy", units);

        ArrayList<Unit> infantryUnits = army.getInfantryUnits();
        for(Unit unit: infantryUnits) assertInstanceOf(InfantryUnit.class, unit);
        assertTrue(infantryUnits.size() > 0);

        ArrayList<Unit> rangedUnits = army.getRangedUnits();
        for(Unit unit: rangedUnits) assertInstanceOf(RangedUnit.class, unit);
        assertTrue(rangedUnits.size() > 0);

        ArrayList<Unit> cavalryUnits = army.getCavalryUnits();
        for(Unit unit: cavalryUnits) assertInstanceOf(CavalryUnit.class, unit);
        assertTrue(cavalryUnits.size() > 0);

        ArrayList<Unit> commanderUnits = army.getCommanderUnits();
        for(Unit unit: commanderUnits) assertInstanceOf(CommanderUnit.class, unit);
        assertTrue(commanderUnits.size() > 0);

    }
}