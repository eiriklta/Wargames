package Game.War.Units.SpecialUnits;

import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Units.SpecialUnits.RangedUnit;
import Game.War.Terrain;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class RangedUnitTest {

    RangedUnit ranged;
    Terrain terrain;

    @BeforeEach
    public void instantiateRangedUnit() {
        ranged = new RangedUnit("Ranged", 10);
    }

    @ParameterizedTest
    @CsvSource({"'', 1, 1, 1",
            "invalidHealth, -1, 1, 1",
            "invalidAttack, 1, -1, 1",
            "invalidArmor, 1, 1, -1"})
    public void constructor_throws_IllegalArgumentException_for_illegal_values(String name, int health, int attack, int armor) {
        assertThrows(IllegalArgumentException.class, () -> {
            var ranged = new InfantryUnit(name, health, attack, armor);
        });
    }

    @Test
    @DisplayName("Attack bonus is constant")
    public void ranged_attackBonus_is_constant() {
        int attackBonus1 = ranged.getAttackBonus(terrain);
        int attackBonus2 = ranged.getAttackBonus(terrain);

        assertEquals(attackBonus1, attackBonus2);
    }

    @Test
    @DisplayName("Ranged unit resist bonus is reduced first two times it is attacked")
    public void ranged_resistBonus_is_reduced() {
        int resistBonus1 = ranged.getResistBonus(terrain);
        int resistBonus2 = ranged.getResistBonus(terrain);
        int resistBonus3 = ranged.getResistBonus(terrain);
        int resistBonus4 = ranged.getResistBonus(terrain);

        assertTrue(resistBonus1 > resistBonus2);
        assertTrue(resistBonus2 > resistBonus3);
        assertEquals(resistBonus3,resistBonus4);
    }

    @ParameterizedTest
    @CsvSource({"0, 96",
                "1, 94",
                "2, 92",
                "3, 92"})
    public void attack_deals_correct_damage(int timesDefended, int expectedHealth) {
        var unit1 = new RangedUnit("unit1", 100);
        var unit2 = new RangedUnit("unit2", 100);

        unit2.setTimesDefended(timesDefended);
        unit1.attackOpponent(unit2, terrain);

        assertEquals(expectedHealth, unit2.getHealth());
    }

    @Test
    public void controlBonuses(){
        RangedUnit ranged = new RangedUnit("Ranged",100);

        assertEquals(1, ranged.getAttackBonus(Terrain.FOREST));

        assertEquals(3, ranged.getAttackBonus(Terrain.PLAINS));

        assertEquals(5,ranged.getAttackBonus(Terrain.HILL));

    }

}