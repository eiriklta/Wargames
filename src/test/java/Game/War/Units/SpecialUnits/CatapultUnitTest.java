package Game.War.Units.SpecialUnits;

import Game.War.Terrain;
import Game.War.Units.SpecialUnits.CatapultUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class CatapultUnitTest {
    CatapultUnit catapultUnit;
    Terrain terrain;

    @BeforeEach
    public void instantiateCatapultUnit() {
        catapultUnit = new CatapultUnit("Catapult", 100);
    }

    @ParameterizedTest
    @CsvSource({"'', 1, 1, 1",
            "invalidHealth, -1, 1, 1",
            "invalidAttack, 1, -1, 1",
            "invalidArmor, 1, 1, -1"})
    public void constructor_throws_IllegalArgumentException_for_illegal_values(String name, int health, int attack, int armor) {
        assertThrows(IllegalArgumentException.class, () -> {
            var catapult = new CatapultUnit(name, health, attack, armor);
        });
    }

    @Test
    @DisplayName("Attack bonus is constant")
    public void catapult_attackBonus_is_constant() {
        int attackBonus1 = catapultUnit.getAttackBonus(terrain);
        int attackBonus2 = catapultUnit.getAttackBonus(terrain);

        assertEquals(attackBonus1, attackBonus2);
    }

    @Test
    @DisplayName("Resist bonus is constant")
    public void catapult_resistBonus_is_constant() {
        int resistBonus1 = catapultUnit.getResistBonus(terrain);
        int resistBonus2 = catapultUnit.getResistBonus(terrain);

        assertEquals(resistBonus1, resistBonus2);
    }

    @Test
    @DisplayName("Attack deals correct damage")
    public void attack_deals_correct_damage() {
        var unit1 = new CatapultUnit("unit1", 100);
        var unit2 = new CatapultUnit("unit2", 100);
        int expectedHealth = 51;
        unit1.attackOpponent(unit2, terrain);

        assertEquals(expectedHealth, unit2.getHealth());
    }

    @Test
    public void controlBonuses() {
        // Controls bonuses for different terrain types
        CatapultUnit catapult = new CatapultUnit("Catapult", 100);

        Assertions.assertEquals(0, catapult.getAttackBonus(Terrain.FOREST));
        Assertions.assertEquals(-10, catapult.getResistBonus(Terrain.FOREST));

        Assertions.assertEquals(0, catapult.getAttackBonus(Terrain.HILL));
        Assertions.assertEquals(-10, catapult.getResistBonus(Terrain.HILL));

        Assertions.assertEquals(20, catapult.getAttackBonus(Terrain.PLAINS));
        Assertions.assertEquals(0, catapult.getResistBonus(Terrain.PLAINS));
    }
}

