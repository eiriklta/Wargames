package Game.War.Units.SpecialUnits;

import Game.War.Units.SpecialUnits.CavalryUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Terrain;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {

    CavalryUnit cavalry;
    Terrain terrain;

    @BeforeEach
    public void instantiateCavalryUnit() {
        cavalry = new CavalryUnit("Cavalry", 10);
    }

    @ParameterizedTest
    @CsvSource({"'', 1, 1, 1",
            "invalidHealth, -1, 1, 1",
            "invalidAttack, 1, -1, 1",
            "invalidArmor, 1, 1, -1"})
    public void constructor_throws_IllegalArgumentException_for_illegal_values(String name, int health, int attack, int armor) {
        assertThrows(IllegalArgumentException.class, () -> {
            var cavalry = new InfantryUnit(name, health, attack, armor);
        });
    }

    @Test
    @DisplayName("Attack bonus is higher first attack")
    public void cavalry_attackBonus_is_higher_first_time() {
        int attackBonus1 = cavalry.getAttackBonus(terrain);
        int attackBonus2 = cavalry.getAttackBonus(terrain);
        int attackBonus3 = cavalry.getAttackBonus(terrain);

        assertTrue(attackBonus1 > attackBonus2);
        assertEquals(attackBonus2, attackBonus3);
    }

    @Test
    @DisplayName("Resist bonus is constant")
    public void cavalry_resistBonus_is_constant() {
        int resistBonus1 = cavalry.getResistBonus(terrain);
        int resistBonus2 = cavalry.getResistBonus(terrain);

        assertEquals(resistBonus1, resistBonus2);
    }

    @ParameterizedTest
    @CsvSource({"0, 87",
                "1, 91",
                "2, 91"})
    public void attack_deals_correct_damage(int timesAttacked, int expectedHealth) {
        var unit1 = new CavalryUnit("Unit1", 100);
        var unit2 = new CavalryUnit("Unit2", 100);
        unit1.setTimesAttacked(timesAttacked);
        unit1.attackOpponent(unit2, terrain);

        assertEquals(expectedHealth, unit2.getHealth());
    }

    @Test
    public void controlBonuses(){
        // Controls bonuses for different terrain types.
        CavalryUnit cavalry = new CavalryUnit("Cavalry",100);

        assertEquals(10,cavalry.getAttackBonus(Terrain.PLAINS));
        assertEquals(1,cavalry.getResistBonus(Terrain.PLAINS));

        assertEquals(2,cavalry.getAttackBonus(Terrain.FOREST));
        assertEquals(0,cavalry.getResistBonus(Terrain.FOREST));

        assertEquals(2,cavalry.getAttackBonus(Terrain.HILL));
        assertEquals(1,cavalry.getResistBonus(Terrain.HILL));
    }


}