package Game.War.Units.SpecialUnits;

import Game.War.Units.SpecialUnits.CommanderUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Terrain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CommanderUnitTest {

    CommanderUnit commander;
    Terrain terrain;

    @BeforeEach
    public void instantiateCavalryUnit() {
        commander = new CommanderUnit("Commander", 10);
    }

    @ParameterizedTest
    @CsvSource({"'', 1, 1, 1",
            "invalidHealth, -1, 1, 1",
            "invalidAttack, 1, -1, 1",
            "invalidArmor, 1, 1, -1"})
    public void constructor_throws_IllegalArgumentException_for_illegal_values(String name, int health, int attack, int armor) {
        assertThrows(IllegalArgumentException.class, () -> {
            var commander = new InfantryUnit(name, health, attack, armor);
        });
    }

    @ParameterizedTest
    @CsvSource({"0, 85",
            "1, 89",
            "2, 89"})
    public void attack_deals_correct_damage(int timesAttacked, int expectedHealth) {
        var unit1 = new CommanderUnit("Unit1", 100);
        var unit2 = new CommanderUnit("Unit2", 100);
        unit1.setTimesAttacked(timesAttacked);
        unit1.attackOpponent(unit2, terrain);

        assertEquals(expectedHealth, unit2.getHealth());
    }

}