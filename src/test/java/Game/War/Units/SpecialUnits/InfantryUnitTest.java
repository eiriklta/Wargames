package Game.War.Units.SpecialUnits;

import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Terrain;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InfantryUnitTest {

    InfantryUnit infantry;
    Terrain terrain;

    @BeforeEach
    public void instantiateInfantryUnit() {
        infantry = new InfantryUnit("Infantry", 10);
    }

    @ParameterizedTest
    @CsvSource({"'', 1, 1, 1",
                "invalidHealth, -1, 1, 1",
                "invalidAttack, 1, -1, 1",
                "invalidArmor, 1, 1, -1"})
    public void constructor_throws_IllegalArgumentException_for_illegal_values(String name, int health, int attack, int armor) {
        assertThrows(IllegalArgumentException.class, () -> {
            var infantry = new InfantryUnit(name, health, attack, armor);
        });
    }

    @Test
    @DisplayName("Attack bonus is constant")
    public void infantry_attackBonus_is_constant() {
        int attackBonus1 = infantry.getAttackBonus(terrain);
        int attackBonus2 = infantry.getAttackBonus(terrain);

        assertEquals(attackBonus1, attackBonus2);
    }

    @Test
    @DisplayName("Resist bonus is constant")
    public void infantry_resistBonus_is_constant() {
        int resistBonus1 = infantry.getResistBonus(terrain);
        int resistBonus2 = infantry.getResistBonus(terrain);

        assertEquals(resistBonus1, resistBonus2);
    }

    @Test
    public void attack_deals_correct_damage() {
        var unit1 = new InfantryUnit("unit1", 100);
        var unit2 = new InfantryUnit("unit2", 100);
        int expectedHealth = 94;
        unit1.attackOpponent(unit2, terrain);

        assertEquals(expectedHealth, unit2.getHealth());
    }

    @Test
    public void controlBonuses() {
        // Controls bonuses for different terrain types.
        InfantryUnit infantry = new InfantryUnit("Infantry",100);

        Assertions.assertEquals(4,infantry.getAttackBonus(Terrain.FOREST));
        Assertions.assertEquals(3,infantry.getResistBonus(Terrain.FOREST));

        Assertions.assertEquals(2,infantry.getAttackBonus(Terrain.HILL));
        Assertions.assertEquals(1,infantry.getResistBonus(Terrain.HILL));

        Assertions.assertEquals(2,infantry.getAttackBonus(Terrain.PLAINS));
        Assertions.assertEquals(1,infantry.getResistBonus(Terrain.PLAINS));

    }
}