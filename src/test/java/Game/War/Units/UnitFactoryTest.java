package Game.War.Units;

import Game.War.Units.SpecialUnits.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnitFactoryTest {

    UnitFactory unitFactory;

    @Test
    public void createUnitTest() {
        unitFactory = new UnitFactory();
        Unit infantryUnit = unitFactory.createUnit("Infantry Unit", "Infantry", 100);
        Unit rangedUnit = unitFactory.createUnit("Ranged Unit", "Ranged", 100);
        Unit cavalryUnit = unitFactory.createUnit("Cavalry Unit", "Cavalry", 100);
        Unit commanderUnit = unitFactory.createUnit("Commander Unit", "Commander", 100);
        Unit catapultUnit = unitFactory.createUnit("Catapult Unit", "Catapult", 100);

        Assertions.assertInstanceOf(InfantryUnit.class, infantryUnit);
        Assertions.assertEquals("Infantry", infantryUnit.getName());
        Assertions.assertEquals(100, infantryUnit.getHealth());

        Assertions.assertInstanceOf(RangedUnit.class, rangedUnit);
        Assertions.assertEquals("Ranged", rangedUnit.getName());
        Assertions.assertEquals(100, rangedUnit.getHealth());

        Assertions.assertInstanceOf(CavalryUnit.class, cavalryUnit);
        Assertions.assertEquals("Cavalry", cavalryUnit.getName());
        Assertions.assertEquals(100, cavalryUnit.getHealth());

        Assertions.assertInstanceOf(CommanderUnit.class, commanderUnit);
        Assertions.assertEquals("Commander", commanderUnit.getName());
        Assertions.assertEquals(100, commanderUnit.getHealth());

        Assertions.assertInstanceOf(CatapultUnit.class, catapultUnit);
        Assertions.assertEquals("Catapult", catapultUnit.getName());
        Assertions.assertEquals(100, catapultUnit.getHealth());
    }

}