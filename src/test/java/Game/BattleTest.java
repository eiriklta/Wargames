package Game;

import Game.War.Army;
import Game.War.Battle;
import Game.War.Terrain;
import Game.War.Units.SpecialUnits.CatapultUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Units.Unit;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class BattleTest {

    Army army;
    Terrain terrain;

    @BeforeEach
    public void initializeArmy() {
        InfantryUnit unit = new InfantryUnit("Infantry", 10);
        ArrayList<Unit> units = new ArrayList<Unit>();
        units.add(unit);
        army = new Army("army", units);
    }

    @Test
    public void exception_is_thrown_if_any_army_is_empty() {
        Army emptyArmy = new Army("Empty army");

        assertThrows(IllegalArgumentException.class, () -> {
            new Battle(army, emptyArmy);
        });
    }

    @Test
    public void exception_if_equal() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Battle(army, army, terrain);
        });
    }


    @Test
    public void check_if_winning_army_is_assigned_correctly() {
        Army army2 = new Army("Army2");
        army2.addUnit(new InfantryUnit("Infantry", 1000));
        Battle battle = new Battle(army, army2, terrain);
        Army winner = battle.simulate();
        assertEquals(winner, army2);
    }

    @Test
    public void check_that_losing_army_has_no_units_left() {
        Army army2 = new Army("Army2");
        army2.addUnit(new InfantryUnit("Infantry", 1000));
        Battle battle = new Battle(army, army2, terrain);
        battle.simulate();
        Assertions.assertFalse(army.hasUnits());
    }
}