import Game.War.Army;
import Game.War.Battle;
import Game.War.Terrain;
import Game.War.Units.SpecialUnits.CatapultUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Units.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class BattleTest {

    Army army;
    Terrain terrain;

    @BeforeEach
    public void initializeArmy() {
        InfantryUnit unit = new InfantryUnit("Infantry", 10);
        ArrayList<Unit> units = new ArrayList<Unit>();
        units.add(unit);
        army = new Army("army", units);
    }

    @Test
    public void exception_is_thrown_if_any_army_is_empty() {
        Army emptyArmy = new Army("Empty army");

        assertThrows(IllegalArgumentException.class, () -> {
            new Battle(army, emptyArmy);
        });
    }


}