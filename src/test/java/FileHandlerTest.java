import Game.War.Units.SpecialUnits.CavalryUnit;
import Game.War.Units.SpecialUnits.CommanderUnit;
import Game.War.Units.SpecialUnits.InfantryUnit;
import Game.War.Units.SpecialUnits.RangedUnit;
import Game.War.Army;
import Game.Client.FileHandler;
import Game.War.Units.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class FileHandlerTest {

    Army army;
    FileHandler fileHandler;

    @BeforeEach
    public void instantiateNewArmy() {
        army = new Army("Army");

        ArrayList<Unit> units = new ArrayList<>();

       for(int i = 0; i < 10; i++) {
           units.add(new InfantryUnit("Infantry", 10));
           units.add(new RangedUnit("Ranged", 10));
           units.add(new CavalryUnit("Cavalry", 10));
           units.add(new CommanderUnit("Commander", 10));
       }

       army.addAll(units);
    }

    @Test
    public void writing_to_file_works() {
        assertDoesNotThrow(() -> fileHandler.armyToCsv("src/test/resources/result.csv", army));
        assertTrue(Files.exists(Path.of("src/test/resources/result.csv")));
    }

    @Test
    public void exception_thrown_if_army_doesnt_exist() {
        assertThrows(NullPointerException.class, () -> fileHandler.armyToCsv("src/test/resources/result.csv", null));
    }

    @Test
    public void exception_thrown_if_file_is_not_csv() {
        assertThrows(IllegalArgumentException.class, () -> fileHandler.armyToCsv("src/test/resources/result.csv.pdf", army));
    }
}