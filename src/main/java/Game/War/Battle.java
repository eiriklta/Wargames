package Game.War;

import Game.War.Units.Unit;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class that contains methods for simulating a battle between two armies in a set terrain.
 *
 * @author Eirik Leer Talstad
 */
public class Battle {

    private final Army armyOne;
    private final Army armyTwo;
    private Terrain terrain;
    private ArrayList<String> battleLog;

    /**
     * Initializes a new battle without a terrain set.
     * Used for testing purposes.
     * @param armyOne Army one
     * @param armyTwo Army two
     * @throws IllegalArgumentException If one one the armies is empty, or if the armies are equal.
     */
    public Battle(Army armyOne, Army armyTwo) throws IllegalArgumentException {
        if(!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("Army cannot be empty");
        if(armyOne.equals(armyTwo)) throw new IllegalArgumentException("Army one cannot be equal to army two!");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = null;
    }

    /**
     * Initializes a new battle with a set terrain.
     * Also creates a battle log which stores a string representation of every attack in the battle.
     * @param armyOne Army one
     * @param armyTwo Army two
     * @param terrain Terrain of the battle
     * @throws IllegalArgumentException
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) throws IllegalArgumentException {
        if(!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("Army cannot be empty");
        if(armyOne.equals(armyTwo)) throw new IllegalArgumentException("Army one cannot be equal to army two!");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
        this.battleLog = new ArrayList<>();
    }

    /**
     * Method that simulates a battle between army one and army two.
     * Randomly chooses which army attacks, and which army defends.
     * Random unit from attacking army is picked, and then attacks random unit from defending army.
     * If the defenders health is zero or below, it is removed from the defending army.
     * If it survives the attack, it attacks the attacker back.
     * If the attacker dies, it is then removed from the attacking army.
     * Battle keeps going until one of the armies has no units left.
     * Winner army is then the army which still has units left.
     * @return Army - winner of the battle
     */

    public Army simulate() {
        int roundCounter = 0;

        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            roundCounter++;
            Army attackers;
            Army defenders;

            if(new Random().nextBoolean()) {
                attackers = armyOne;
                defenders = armyTwo;
            } else {
                attackers = armyTwo;
                defenders = armyOne;
            }

            Unit attacker = attackers.getRandom();
            Unit defender = defenders.getRandom();
            int damage = attacker.attackOpponent(defender, terrain);
            battleLog.add(attackLog(roundCounter, damage, attacker, defender, attackers, defenders));
            if(defender.getHealth() <= 0) {
                defenders.removeUnit(defender);
                } else {
                damage = defender.attackOpponent(attacker, terrain);
                battleLog.add(attackLog(roundCounter, damage, attacker, defender, attackers, defenders));
                if(attacker.getHealth() <= 0) {
                    attackers.removeUnit(attacker);
                }
            }
            }
            if(armyOne.hasUnits()) {
                battleLog.add(armyOne.getName() + " wins the battle!");
                return armyOne;
            } else {
                battleLog.add(armyTwo.getName() + " wins the battle!");
                return armyTwo;
            }
        }

    /**
     * String representation of an attack done in the battle simulation.
      * @param roundNumber
     * @param damageDealt
     * @param attacker
     * @param defender
     * @param attackerArmy
     * @param defenderArmy
     * @return
     */
    public String attackLog(int roundNumber, int damageDealt, Unit attacker, Unit defender, Army attackerArmy, Army defenderArmy) {
        return ("Round: "+ roundNumber + "\n " + attacker.getName() + " from " + attackerArmy.getName() + " attacks and deals " +
                damageDealt + " damage to " + "\n" + defender.getName() + " from " + defenderArmy.getName() + ".\n" +
                defender.getName() + " health is now at " + defender.getHealth());
    }

    /**
     * Method for getting the battle log.
     * @return
     */
    public ArrayList<String> getBattleLog() {
        return battleLog;
    }

    /**
     * Method for getting terrain type.
     * @return
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Method for setting terrain type.
     * @param terrain
     */
    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    /**
     * String presentation of the battle.
     * @return String
     */
    @Override
    public String toString() {
        return "Game.Game.Battle.Battle: " + '\n' + armyOne + '\n' + "VS" + '\n' + armyTwo;
    }
}
