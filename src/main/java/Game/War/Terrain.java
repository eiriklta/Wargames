package Game.War;

/**
 * Enum class that represents constants for different terrain types.
 */
public enum Terrain {
    PLAINS,
    HILL,
    FOREST
}
