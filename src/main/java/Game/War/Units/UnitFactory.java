package Game.War.Units;

import Game.War.Units.SpecialUnits.*;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Factory design pattern that creates different units depending on the given parameters.
 * Useful for creating large amounts of one type of unit.
 */
public class UnitFactory {

    /**
     * Method creates a given unit type with name and health given as parameters.
     * @param type
     * @param name
     * @param health
     * @return
     */
    public Unit createUnit(String type, String name, int health) {
        if(type == null || type.isEmpty())
            return null;

        type = type.toUpperCase(Locale.ROOT);
        type = type.replaceAll(" ", "");
        switch(type) {
            case "INFANTRYUNIT":
                return new InfantryUnit(name, health);
            case "RANGEDUNIT":
                return new RangedUnit(name, health);
            case "CAVALRYUNIT":
                return new CavalryUnit(name, health);
            case "COMMANDERUNIT":
                return new CommanderUnit(name, health);
            case "CATAPULTUNIT":
                return new CatapultUnit(name, health);
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }

    /**
     * Method for creating a given amount of units. Unit type, name and health are passed as parameters.
     * @param type
     * @param name
     * @param health
     * @param amount
     * @return
     * @throws IllegalArgumentException
     */
    public ArrayList<Unit> returnUnits(String type, String name, int health, int amount) throws IllegalArgumentException {
        if(amount < 0) throw new IllegalArgumentException("Amount cannot be a negative number!");
        ArrayList<Unit> units = new ArrayList<>();
        for(int i = 0; i < amount; i++) {
            units.add(createUnit(type, name, health));
        }
        return units;
    }

    /**
     * Method for copying an array of units to a new array.
     * @param units
     * @return
     */
    public ArrayList<Unit> copyUnits(ArrayList<Unit> units) {
        ArrayList<Unit> copiedUnits = new ArrayList<>();
        for(Unit unit: units) {
            copiedUnits.add(createUnit(unit.getUnitType(), unit.getName(), unit.getHealth()));
        }
        return copiedUnits;
    }
}