    package Game.War.Units;

    import Game.War.Terrain;

    /**
     * The abstract class Unit.
     * All units that are created in the simulation are subclasses of Unit.
     *
     * @author eleer
     */
    public abstract class Unit {

    private final String name;
    public int health;
    private final int attack;
    private final int armor;

        /**
         * Constructs an Unit object. Checks if namy is empty, and if health, attack or armor have illegal values.
         * @param name String - short name describing the unit.
         * @param health int - health of the unit
         * @param attack - damage done on enemy units, excluding attack bonus.
         * @param armor - damage resisted from enemy units, excluding resist bonus.
         * @throws IllegalArgumentException if name is empty, or health is zero or less than zero
         */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if(name.isBlank()) throw new IllegalArgumentException("Name cannot be blank");
        this.name = name;
        if(health <= 0) throw new IllegalArgumentException("Health cannot not be less than 0 or equal to 0");
        this.health = health;
        if(attack < 0) throw new IllegalArgumentException("Attack cannot be negative");
        this.attack = attack;
        if(armor < 0) throw new IllegalArgumentException("Armor cannot be negative");
        this.armor = armor;
    }

        /**
         * Method for attacking enemy unit
         * Uses the enemy units health, armor and resist bonus to calculate how much health the opponent has left after the attack.
         * The formula for this calculation is:
         *
         * health - (attack + attackBonus) + (armor + resistBonus)
         *
         * @param opponent Unit - opponent of any subclass
         */
    public int attackOpponent(Unit opponent, Terrain terrain) {
        opponent.setHealth(opponent.getHealth() - (getAttack() + getAttackBonus(terrain)) + (opponent.getArmor() + opponent.getResistBonus(terrain)));
        return (getAttack() + getAttackBonus(terrain)) + (opponent.getArmor() + opponent.getResistBonus(terrain));
    }

        /**
         * Method to get the name of the unit
         * @return name of the unit
         */
    public String getName() {
        return name;
    }

    public String getUnitType() {
        return getClass().getSimpleName();
    }

        /**
         * Method to get the health of the unit
         * @return health of the unit
         */
    public int getHealth() {
        return health;
    }

        /**
         * Sets the health of the unit.
         * @param health int - health points of the unit
         */
    public void setHealth(int health) {
        this.health = health;
    }

        /**
         * Gets attack damage of the unit.
         * @return attack damage.
         */
    public int getAttack() {
        return attack;
    }

        /**
         * Gets armor of the unit.
         * @return armor
         */
    public int getArmor() {
        return armor;
    }

        /**
         * Method to get a text string describing the unit.
         * Overrides the default toString method of the Object superclass.
         * @return String describing the unit.
         */
    @Override
    public String toString() {
        return getUnitType() + " | Name: " + name + " | HP: " + health;
    }

        /**
         * Gets attack bonus.
         * Method is abstract, and has to be defined in subclasses.
         * @return attack bonus
         */
    public abstract int getAttackBonus(Terrain terrain);

        /**
         * Gets resist bonus.
         * Method is abstract, and has to be defined in subclasses.
         * @return resist bonus
         * @param terrain
         */
    public abstract int getResistBonus(Terrain terrain);
}
