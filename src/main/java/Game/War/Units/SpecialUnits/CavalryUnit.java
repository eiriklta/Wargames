package Game.War.Units.SpecialUnits;

import Game.War.Units.Unit;
import Game.War.Terrain;

/**
 * Class CavalryUnit that inherits from Unit superclass.
 * Represents a cavalry unit.
 */
public class CavalryUnit extends Unit {

    private int timesAttacked;

    /**
     * Constructor that instantiates a new CavalryUnit object
     * Uses the constructor of the superclass Unit
     *
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor that uses the constructor of the Unit superclass to instantiate a new CavalryUnit
     * Attack is set to 20
     * Armor is set to 12
     *
     * @param name
     * @param health
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    public void setTimesAttacked(int timesAttacked) {
        this.timesAttacked = timesAttacked;
    }

    /**
     * Overrides abstract method getAttackBonus() from superclass Unit.
     * The value of attackBonus is calculated based on how many this unit has attacked an enemy unit.
     * The first time it attacks an enemy it performs a charge attack which has an increased damage bonus of 4.
     * After the charge attack, every attack does 2 damage.
     *
     * @return attackBonus - value of the attackBonus
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        if(terrain == Terrain.PLAINS) {
            if(timesAttacked == 0) {
                timesAttacked++;
                return 10;
            } else {
                return 4;
            }
        } else {
            if(timesAttacked == 0) {
                timesAttacked++;
                return 6;
            } else {
                return 2;
            }
        }
    }

    /**
     * Overrides abstract method getResistBonus() from superclass Unit.
     * The int resistBonus is constant.
     *
     * @return resistBonus
     * @param terrain
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? 0 : 1;
    }
}
