package Game.War.Units.SpecialUnits;

import Game.War.Units.Unit;
import Game.War.Terrain;

/**
 * Class RangedUnit that inherits from Unit superclass.
 * Represents a ranged unit
 */
public class RangedUnit extends Unit {

    private int timesDefended;

    /**
     * Constructor that instantiates a new RangedUnit object
     * Uses the constructor of the superclass Unit
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor that uses the constructor of the Unit superclass to instantiate a new InfantryUnit
     *   Attack is set to 15
     *   Armor is set to 8
     * @param name
     * @param health
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * Overrides abstract method getAttackBonus() from superclass Unit.
     * The int attackBonus is constant.
     *
     * @return 3 - which is the attackBonus of this unit type.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        if(terrain == Terrain.HILL) return 5;
        if(terrain == Terrain.FOREST) return 1;
        else return 3;
    }

    /**
     * Overrides abstract method getResistBonus() from superclass Unit.
     * The value of resistBonus is calculated based on how many times this unit has been attacked.
     * Resist bonus starts off at 6, and is reduced by 2 each time it is attacked until resistBonus reaches 2.
     * From then on the resistBonus is constant at 2.
     *
     * @return int - value of the resist bonus
     * @param terrain
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int resistBonus = timesDefended >= 2 ? 2 : 6-2* timesDefended;
        timesDefended++;
        return resistBonus;
    }

    public void setTimesDefended(int timesDefended) {
        this.timesDefended = timesDefended;
    }
}
