package Game.War.Units.SpecialUnits;

import Game.War.Terrain;
import Game.War.Units.Unit;

/**
 * Class CatapultUnit that inherits from class Unit.
 * Represents a catapult unit.
 */
public class CatapultUnit extends Unit {

    /**
     * Constructor that instantiates a new CatapultUnit object.
     * Uses the constructor of the superclass Unit
     * @param name String - a short name describing the unit.
     * @param health int - amount of health that the unit has.
     * @param attack int - damage that the unit does to other units.
     * @param armor int - damage that the unit resists
     */
    public CatapultUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor with attack and armor already predefined.
     * @param name String - a short name describing the unit.
     * @param health int - amount of health the unit has
     */
    public CatapultUnit(String name, int health) {
        super(name, health, 50, 1);
    }

    /**
     * Overrides abstract method getAttackBonus() from superclass Unit.
     * Attack bonus is higher if terrain is set to plains.
     * @return
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        return (terrain == Terrain.PLAINS) ? 20 : 0;
    }

    /**
     * Overrides abstract method getResistsBonus() from superclass Unit.
     * Armor of unit is reduced if terrain is set to hill or forest.
     * @return
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return ((terrain == Terrain.HILL) || (terrain == Terrain.FOREST)) ? -10 : 0;
    }
}
