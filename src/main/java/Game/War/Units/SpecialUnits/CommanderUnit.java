package Game.War.Units.SpecialUnits;

/**
 * Class CommanderUnit that inherits from CavalryUnit.
 * Represents a commander unit
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor that instantiates a new CommanderUnit object
     * Uses the constructor of the superclass CavalryUnit
     *
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor that instantiates a new CommanderUnit object.
     * Uses the constructor of the CavalryUnit superclass.
     * Attack value is set to 25
     * Armor value is set to 15
     * @param name String - short name describing the unit
     * @param health int - amount of health the unit has
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }
}
