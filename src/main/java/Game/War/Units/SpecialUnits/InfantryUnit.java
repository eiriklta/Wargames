package Game.War.Units.SpecialUnits;

import Game.War.Units.Unit;
import Game.War.Terrain;

/**
 * Class InfantryUnit that inherits from class Unit.
 * Represents an infantry unit.
 *
 */
public class InfantryUnit extends Unit {

    /**
     * Constructor that instantiates a new InfantryUnit object
     * Uses the constructor of the superclass Unit
     * @param name string - a short name of the unit.
     * @param health int - amount of health that the unit has.
     * @param attack int - damage that the unit does to other units.
     * @param armor int - damage that the unit resists from other units.
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor that uses the constructor of the Unit superclass to instantiate a new InfantryUnit
     * Attack is set to 15
     * Armor is set to 10
     * @param name string - a short name of the unit.
     * @param health int - amount of health that the unit has.
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * Overrides abstract method getAttackBonus() from superclass Unit.
     * Attack bonus higher if terrain is set to forest.
     * @return attackBonus
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? 4 : 2;
    }

    /**
     * Overrides abstract method getResistBonus() from superclass Unit.
     * Resist bonus is higher if terrain is set to forest.
     * @return resistBonus
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? 3 : 1;
    }
}
