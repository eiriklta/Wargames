package Game.War;

import Game.War.Units.*;
import Game.War.Units.SpecialUnits.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class representing an army of units. Contains methods for adding, removing and getting information from units.
 *
 * @author Eirik Leer
 */
public class Army {

        private final String name;
        private final ArrayList<Unit> units;

    /**
     * Constructor that instantiates an object of the army class without any units.
     * @param name String - name of the army.
     */
    public Army(String name) throws IllegalArgumentException {
        this(name, new ArrayList<>());
    }

    /**
     * Constructor that instantiates a new Army object with a list of units.
     * Takes in a name, and a list of units.
     *
     * @param name name of the army as a String
     * @param units List of units
     */
    public Army(String name, ArrayList<Unit> units) throws IllegalArgumentException {
        if(name.isBlank()) throw new IllegalArgumentException("Name cannot be blank");
        this.name = name;
        this.units = units;
    }

    /**
     * Gets name of army.
     *
     * @return name of army as String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets all units from army.
     *
     * @return list of units
     */
    public ArrayList<Unit> getAllUnits() {
        return units;
    }

    /**
     * Method to get all {@link Unit} of type {@link InfantryUnit} using stream and filter
     *
     * @return Infantry units in a list
     */
    public ArrayList<Unit> getInfantryUnits() {
        return units.stream()
                .filter(unit -> unit instanceof InfantryUnit)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method to get all {@link Unit} of type {@link CavalryUnit} using stream and filter
     * Since commander units are also cavalry units, there has to be a check to make sure units are not commander units.
     *
     * @return Cavalry units in a list
     */
    public ArrayList<Unit> getCavalryUnits() {
        return units.stream()
                .filter(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method to get all {@link Unit} of type {@link RangedUnit} using stream and filter
     *
     * @return Ranged units in a list
     */
    public ArrayList<Unit> getRangedUnits() {
        return units.stream()
                .filter(unit -> unit instanceof RangedUnit)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method to get all {@link Unit} of type {@link CommanderUnit} using stream and filter
     *
     * @return Commander units in a list
     */
    public ArrayList<Unit> getCommanderUnits() {
        return units.stream().filter(unit -> unit instanceof CommanderUnit).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method to get all {@link Unit} of type {@link CatapultUnit} using stream and filter
     * @return Catapult units in a list
     */
    public ArrayList<Unit> getCatapultUnits() {
        return units.stream().filter(unit -> unit instanceof CatapultUnit).collect(Collectors.toCollection(ArrayList::new));
    }


    /**
     * Method for adding an unit to the army.
     *
     * @param unit to be added
     * @return true if unit was added successfully, and false if something went wrong.
     */
    public boolean addUnit(Unit unit) {
        return this.units.add(unit);
    }

    /**
     * Method for adding all units from a list to the army.
     *
     * @param units to be added.
     */
    public void addAll(ArrayList<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * Method for removing an unit from the army.
     *
     * @param unit to be removed.
     * @return true if unit was removed successfully, and false if something went wrong.
     */
    public boolean removeUnit(Unit unit) {
        return this.units.remove(unit);
    }

    public void removeAllUnits() {
        for(Iterator<Unit> it = this.units.iterator(); it.hasNext(); ) {
            Unit unit = it.next();
            it.remove();
        }
    }

    /**
     * Method that checks if given list has any elements. Method returns true if it has elements, and returns false if the given list is empty.
     *
     * @return returns true if list has elements, and returns false if list is empty.
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Method for getting a random Unit object from the army.
     *
     * @return random Unit object
     */
    public Unit getRandom() {
        Random random = new Random();
        if(this.hasUnits()) {
            return units.get(random.nextInt(units.size()));
        } else {
            throw new IllegalArgumentException("Army is empty");
        }
    }

    /**
     * Overrides the toString() method from object
     *
     * @return String representation of army
     */
    @Override
    public String toString() {
        return name + '\n' + "units:" + units;
    }

    /**
     * Checks if army is equal to another object.
     *
     * @param o - object to be compared with
     * @return true if objects are the same
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * Generates a hashcode
     *
     * @return hashcode as an int
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
