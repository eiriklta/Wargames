package Game.GUI.Model;

import Game.War.Units.UnitFactory;
import Game.War.Army;
import Game.War.Battle;
import Game.War.Terrain;

/**
 * Singleton class that stores the data which is used during the runtime of the application.
 *
 * Contains methods for mutating and accessing this data.
 */
public class BattleSimulationModel {

    private final static BattleSimulationModel battleSimulation = new BattleSimulationModel();
    private Battle battle;
    private final Army[] initialArmies;
    private final Army[] currentArmies;
    private final int[] armyScores;
    private Terrain terrain;

    /**
     * Constructor that initializes the arrays storing the data in the application.
     */
    private BattleSimulationModel() {
        initialArmies = new Army[2];
        currentArmies = new Army[2];
        armyScores = new int[2];
    }

    /**
     * Gets the instance of the battle simulation model class.
     * @return
     */
    public static BattleSimulationModel getBattleSimulation() {
        return battleSimulation;
    }

    /**
     * Method for setting battle.
     * @param battle
     */
    public void setBattle(Battle battle) {
        this.battle = battle;
    }

    /**
     * Method for getting battle.
     * @return
     */
    public Battle getBattle() {
        return battle;
    }

    /**
     * Method for getting initial army.
     * @param number - number of the army to get.
     * @return Army - initial army
     */
    public Army getInitialArmy(int number) {
        return initialArmies[number-1];
    }

    /**
     * Method for setting initial army
     * @param number - number of the army to set.
     * @param army - Army to set to the initial army.
     */
    public void setInitialArmy(int number, Army army) {
        initialArmies[number-1] = army;
    }

    /**
     * Method for getting current army.
     * @param number - number of the army to get.
     * @return Army - current army
     */
    public Army getCurrentArmy(int number) {
        return currentArmies[number-1];
    }

    /**
     * Method for setting current army
     * @param number - number of the army to set.
     * @param army - Army to set to current army.
     */
    public void setCurrentArmy(int number, Army army) {
        currentArmies[number-1] = army;
    }

    /**
     * Method that utilizes UnitFactory to copy a given army, and return it as a new army.
     * @param army - Army to copy
     * @return Army - copied army
     * @throws NullPointerException if given army is null.
     */
    public Army copyArmy(Army army) throws NullPointerException {
        UnitFactory unitFactory = new UnitFactory();
        return new Army(army.getName(), unitFactory.copyUnits(army.getAllUnits()));
    }

    /**
     * Gets the score of the army.
     * @param number - number of army to get score from.
     * @return int - current score.
     */
    public int getArmyScores(int number) {
        return armyScores[number-1];
    }

    /**
     * Sets the score of the army.
     * @param number - number of the army to set score for.
     * @param score int - which score to set
     */
    public void setArmyScores(int number, int score) {
        this.armyScores[number-1] = score;
    }

    /**
     * Gets battle terrain type
     * @return Terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Sets battle terrain type
     * @param terrain Terrain to set
     */
    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }
}
