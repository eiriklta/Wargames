package Game.GUI.View;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class WargamesApp extends Application {

    private static Stage stage;

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(WargamesApp.class.getClassLoader().getResource("Wargames.fxml"));
        stage = primaryStage;
        stage.setTitle("Wargames");
        Scene scene = new Scene(fxmlLoader.load(), 1200, 850);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static Stage getStage() {
        return stage;
    }

    public static void changeScene(String scene) {
        FXMLLoader fxmlLoader = new FXMLLoader(WargamesApp.class.getClassLoader().getResource(scene));
        try {
            stage.getScene().setRoot(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
