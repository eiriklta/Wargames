package Game.GUI.Controllers;

import Game.GUI.Model.BattleSimulationModel;
import Game.Client.FileHandler;
import Game.GUI.View.WargamesApp;
import Game.War.Units.Unit;
import Game.War.Army;
import Game.War.Battle;
import Game.War.Terrain;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

/**
 * Controller that handles the user input in the fxml file Wargames.fxml.
 *
 * Contains methods for importing an army, resetting armies and removing armies from the application.
 *
 * Also includes methods for starting a new battle, choosing battle terrain and for displaying the armies before and after a battle is fought.
 */
public class WargamesController implements Initializable {

    @FXML private Label scoreArmyOneLabel, scoreArmyTwoLabel;
    @FXML private Label totalUnitsArmyOneLabel, totalUnitsArmyTwoLabel;
    @FXML private Label infantryArmyOneLabel, infantryArmyTwoLabel;
    @FXML private Label rangedArmyOneLabel, rangedArmyTwoLabel;
    @FXML private Label cavalryArmyOneLabel, cavalryArmyTwoLabel;
    @FXML private Label commanderArmyOneLabel, commanderArmyTwoLabel;
    @FXML private Label catapultArmyOneLabel, catapultArmyTwoLabel;
    @FXML private ListView<Unit> armyOneUnits, armyTwoUnits;
    @FXML private TextField filepathArmyOne, filepathArmyTwo;
    @FXML private TitledPane overviewArmyOne, overviewArmyTwo;

    @FXML private AnchorPane background;
    @FXML private TextArea battleLogTextArea;

    private final BattleSimulationModel battleSimulation = BattleSimulationModel.getBattleSimulation();
    private boolean simulationDone = true;

    /**
     * Creates a new battle with the armies currently registered in the application, and starts the battle simulation.
     * While simulation is being run, a log of all the attacks happening in the battle is being updated and displayed in the GUI.
     * The remaining units of the winning army are displayed with their current health values,
     * and the score of the winning army is incremented by one.
     *
     * If one or both of the armies are empty, there is an error message displayed with an explanation of the error.
     * If the button for starting a battle is pressed while the simulation is happening, a warning is displayed.
     *
     */
    @FXML public void startBattle() {
            if(simulationDone) {
                try {
                    battleSimulation.setCurrentArmy(1, battleSimulation.copyArmy(battleSimulation.getInitialArmy(1)));
                    battleSimulation.setCurrentArmy(2, battleSimulation.copyArmy(battleSimulation.getInitialArmy(2)));
                    battleSimulation.setBattle(new Battle(battleSimulation.getCurrentArmy(1), battleSimulation.getCurrentArmy(2), battleSimulation.getTerrain()));
                } catch (NullPointerException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "You have to import or create an army for both sides!");
                    alert.showAndWait();
                }

                try {
                    Army winnerArmy = battleSimulation.getBattle().simulate();
                    runSimulation();
                    showArmy(1, battleSimulation.getCurrentArmy(1));
                    showArmy(2, battleSimulation.getCurrentArmy(2));

                    if(winnerArmy.equals(battleSimulation.getCurrentArmy(1))) {
                        battleSimulation.setArmyScores(1, battleSimulation.getArmyScores(1) + 1);
                        scoreArmyOneLabel.setText(String.valueOf(battleSimulation.getArmyScores(1)));

                    } else {
                        battleSimulation.setArmyScores(2, battleSimulation.getArmyScores(2) + 1);
                        scoreArmyTwoLabel.setText(String.valueOf(battleSimulation.getArmyScores(2)));
                    }
                } catch (IllegalArgumentException e) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage());
                    alert.showAndWait();
                } catch (NullPointerException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Simulation is not done");
                alert.showAndWait();
            }
    }

    /**
     * Helper method for startBattle() that runs the simulation of the battle log.
     * A thread is started that executes the simulation.
     * Uses an iterator to iterate through the elements of the battle log, and displays them in a list in the GUI.
     * While this is running the boolean simulationDone is set to false, and when it is done the boolean is set to true.
     * This is so that other button can`t be pressed while it is running, and possibly disturbing the program flow.
     */
    private void runSimulation() {
        simulationDone = false;
        int sleep = 150;
        Thread thread = new Thread(() -> {
            Runnable updater = () -> {
            };
            ArrayList<String> battleLog = battleSimulation.getBattle().getBattleLog();
            Iterator<String> iterator =  battleLog.iterator();
            String print = null;
            while(iterator.hasNext()) {
                try {
                    print = iterator.next() + "\n";
                    battleLogTextArea.appendText(print);
                    Thread.sleep(sleep);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Platform.runLater(updater);
            }
            simulationDone = true;
            showArmy(1, battleSimulation.getCurrentArmy(1));
            showArmy(2, battleSimulation.getCurrentArmy(2));
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Method for importing an army.
     * Utilizes FileHandler class to open file explorer, and get path of chosen file.
     * File is read by FileHandler and exported into an Army object.
     * Information about imported army is displayed in GUI.
     * Alert pops up if something is wrong with the file, or if there was an error while reading file.
     *
     * @param armyNumber Number of army. 1 for Army one, 2 for Army two.
     */
    private void importArmy(int armyNumber) {
        FileHandler fileHandler = new FileHandler();
        String filePath = fileHandler.getFilePath();
        try {
            battleSimulation.setInitialArmy(armyNumber, fileHandler.readArmyFromCsv(filePath));
            if(armyNumber == 1) filepathArmyOne.setText(filePath);
            else filepathArmyTwo.setText(filePath);
            showArmy(armyNumber, battleSimulation.getInitialArmy(armyNumber));
        } catch (IllegalArgumentException e) {
           Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
           alert.showAndWait();
        } catch (ArrayIndexOutOfBoundsException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Unit format is invalid");
            alert.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Something went wrong");
            alert.showAndWait();
        }
            scoreArmyOneLabel.setText(String.valueOf(0));
            scoreArmyTwoLabel.setText(String.valueOf(0));
        }

    /**
     * Handles event for importing Army one
     * @param event
     */
    @FXML public void importArmyOne(ActionEvent event) {
        importArmy(1);
    }

    /**
     * Handles event for importing Army two
     * @param event
     */
    @FXML public void importArmyTwo(ActionEvent event) {
        importArmy(2);
    }

    /**
     * Updates GUI to display information about given army.
     * @param armyNumber - number of army
     * @param army - selected army to display
     */
    public void showArmy(int armyNumber, Army army) {
        if(armyNumber == 1) {
            overviewArmyOne.setText(army.getName());
            totalUnitsArmyOneLabel.setText(String.valueOf(army.getAllUnits().size()));
            infantryArmyOneLabel.setText(String.valueOf(army.getInfantryUnits().size()));
            rangedArmyOneLabel.setText(String.valueOf(army.getRangedUnits().size()));
            cavalryArmyOneLabel.setText(String.valueOf(army.getCavalryUnits().size()));
            commanderArmyOneLabel.setText(String.valueOf(army.getCommanderUnits().size()));
            catapultArmyOneLabel.setText(String.valueOf(army.getCatapultUnits().size()));
            armyOneUnits.setItems(FXCollections.observableArrayList(army.getAllUnits()));
        } else if (armyNumber == 2) {
            overviewArmyTwo.setText(army.getName());
            totalUnitsArmyTwoLabel.setText(String.valueOf(army.getAllUnits().size()));
            infantryArmyTwoLabel.setText(String.valueOf(army.getInfantryUnits().size()));
            rangedArmyTwoLabel.setText(String.valueOf(army.getRangedUnits().size()));
            cavalryArmyTwoLabel.setText(String.valueOf(army.getCavalryUnits().size()));
            commanderArmyTwoLabel.setText(String.valueOf(army.getCommanderUnits().size()));
            catapultArmyTwoLabel.setText(String.valueOf(army.getCatapultUnits().size()));
            armyTwoUnits.setItems(FXCollections.observableArrayList(army.getAllUnits()));
        } else throw new IllegalArgumentException("Invalid army number");
    }

    /**
     * Resets the armies back to the initial armies, and displays them.
     * If simulation is running while this button is clicked, a warning popup is displayed preventing this action from happening.
     */
    @FXML public void resetArmies() {
        if(simulationDone) {
            battleSimulation.setCurrentArmy(1, battleSimulation.copyArmy(battleSimulation.getInitialArmy(1)));
            showArmy(1, battleSimulation.getCurrentArmy(1));
            battleSimulation.setCurrentArmy(2, battleSimulation.copyArmy(battleSimulation.getInitialArmy(2)));
            showArmy(2, battleSimulation.getCurrentArmy(2));
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Simulation is not done");
            alert.showAndWait();
        }
    }

    /**
     * Changes the scene to the fxml file CreateArmy.fxml
     * This scene contains ways to add units to a new army, and to add this army to the program.
     */
    @FXML  public void createArmyScene() {
        WargamesApp.changeScene("CreateArmy.fxml");
    }

    /**
     * Button for setting battle terrain to hill.
     * Changes the background of the scene to a picture showing what the terrain is.
     */
    @FXML public void chooseTerrainHill() {
        battleSimulation.setTerrain(Terrain.HILL);
        background.setStyle("-fx-background-image: url(hill_background.jpg);" + "-fx-background-size: stretch, stretch");
    }

    /**
     * Button for setting battle terrain to forest.
     * Changes the background of the scene to a picture showing what terrain it is.
     */
    @FXML public void chooseTerrainForest() {
        battleSimulation.setTerrain(Terrain.FOREST);
        background.setStyle("-fx-background-image: url(forest_background.jpg);" + "-fx-background-size: stretch, stretch");
    }

    /**
     * Button for setting battle terrain to plains.
     * Changes the background of the scene to a picture showing what terrain it is.
     */
    @FXML public void chooseTerrainPlains() {
        battleSimulation.setTerrain(Terrain.PLAINS);
        background.setStyle("-fx-background-image: url(plains.jpg);" + "-fx-background-size: stretch, stretch");
    }

    /**
     * Method for removing a registered army.
     * If this method is called while simulation is running, a warning popup is displayed preventing this action from happening.
     * @param armyNumber - number of the army to remove
     */
    public void removeArmy(int armyNumber) {
        if(simulationDone) {
            battleSimulation.getInitialArmy(armyNumber).removeAllUnits();
            showArmy(armyNumber, battleSimulation.getInitialArmy(armyNumber));
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Simulation is still running");
            alert.showAndWait();
        }
    }

    /**
     * Button that calls method for removing army one.
     */
    @FXML public void removeArmyOne() {
        removeArmy(1);
    }

    /**
     * Button that calls method for removing army one.
     */
    @FXML public void removeArmyTwo() {
        removeArmy(2);
    }

    /**
     * Initializes fxml file and controller.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(!(battleSimulation.getInitialArmy(1) == null)) showArmy(1, battleSimulation.getInitialArmy(1));
        if(!(battleSimulation.getInitialArmy(2) == null)) showArmy(2, battleSimulation.getInitialArmy(2));
        }
    }
