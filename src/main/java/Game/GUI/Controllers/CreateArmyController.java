package Game.GUI.Controllers;

import Game.GUI.Model.BattleSimulationModel;
import Game.GUI.View.WargamesApp;
import Game.War.Units.Unit;
import Game.War.Units.UnitFactory;
import Game.War.Army;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller that handles user input in the fxml file CreateArmy.fxml.
 *
 * Contains methods for adding units to a new army, and for initializing this army to a given side.
 */
public class CreateArmyController implements Initializable {

    @FXML private TextField armyNameTextField, unitNameTextField, unitHealthTextField, unitAmountTextField;
    @FXML private ChoiceBox unitTypeChoiceBox = new ChoiceBox();
    @FXML private ListView<Unit> newArmyList;

    private ArrayList<Unit> units = new ArrayList<>();
    BattleSimulationModel battleSimulation = BattleSimulationModel.getBattleSimulation();

    /**
     * Displays a dialog popup that allows you to choose side you want to create an army for.
     */
    @FXML
    public int chooseSide() {
        ArrayList<String> choices = new ArrayList<>();
        choices.add("Army One");
        choices.add("Army Two");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose army", choices);
        dialog.setHeaderText("Choose army:");
        dialog.setContentText("Army:");

        Optional<String> result = dialog.showAndWait();
        if(result.get().equals("Army One")) {
            return 1;
        } else return 2;
    }

    /**
     * Button for creating a new army.
     * Collects army name, army number and a list of units. Then initializes this army, and switches back to previous scene.
     */
    @FXML  public void createArmy() {
        int armyNumber = chooseSide();
        String armyName = armyNameTextField.getText();

        battleSimulation.setInitialArmy(armyNumber, new Army(armyName, units));
        battleSimulation.setArmyScores(1,0);
        battleSimulation.setArmyScores(2,0);
        WargamesApp.changeScene("Wargames.fxml");
    }

    /**
     * Method that utilizes UnitFactory to create units of the specified type with given name, health and amount values.
     * These units are added to a list, which is displayed in an army preview in the GUI.
     */
    @FXML public void addUnits() {
        UnitFactory unitFactory = new UnitFactory();

        String unitType = unitTypeChoiceBox.getValue().toString().replace(" ", "");
        String unitName = unitNameTextField.getText();
        int unitHealth = Integer.parseInt(unitHealthTextField.getText());
        int unitAmount = Integer.parseInt(unitAmountTextField.getText());

        ArrayList<Unit> unitsToAdd = new ArrayList<>();
        unitsToAdd.addAll(unitFactory.returnUnits(unitType, unitName, unitHealth, unitAmount));
        units.addAll(unitsToAdd);
        newArmyList.setItems(FXCollections.observableArrayList(units));
    }

    /**
     * Button that allows you to go back to the main scene.
     */
    @FXML public void goBack() {
        WargamesApp.changeScene("Wargames.fxml");
    }

    /**
     * Initializes fxml file and controller for this scene.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String unitTypes[] = { "Infantry Unit", "Ranged Unit", "Cavalry Unit", "Commander Unit", "Catapult Unit" };
        unitTypeChoiceBox.setItems(FXCollections.observableArrayList(unitTypes));
    }
}
