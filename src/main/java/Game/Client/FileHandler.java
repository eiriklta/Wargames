package Game.Client;

import Game.GUI.View.WargamesApp;
import Game.War.Army;
import Game.War.Units.Unit;
import Game.War.Units.UnitFactory;
import com.opencsv.CSVWriter;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.ArrayList;

/**
 * Class for reading an army from a file, and writing an army to a file.
 *
 * @author Eirik Leer Talstad
 */
public class FileHandler {

    /**
     * Method for reading an army to a csv file.
     * Takes in a filepath and utilizes a filereader to read the contents of the file.
     * Army name and units are then extracted from the file into a valid army.
     * Throws IllegalArgumentException if name is empty, or if name is invalid.
     * Throws ArrayIndexOutOfBoundsException if invaid unit format.
     *
     * @return Army - army that is read from the CSV file.
     */
    public Army readArmyFromCsv(String filePath) throws IOException, IllegalArgumentException, ArrayIndexOutOfBoundsException {
        UnitFactory unitFactory = new UnitFactory();
        ArrayList<Unit> units = new ArrayList<>();

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line;
            String armyName = bufferedReader.readLine();
            if(armyName == null) {
                throw new IllegalArgumentException("Name is empty");
            } else if (armyName.contains(",") || armyName.isEmpty()) {
                throw new IllegalArgumentException("Name format is invalid");
            } else {
                Army army = new Army(armyName, units);
                while ((line = bufferedReader.readLine()) != null) {
                    String[] unit = line.split(",");
                    String unitType = unit[0].trim();
                    String name = unit[1].trim();
                    int health = Integer.parseInt(unit[2].trim());

                    if (unit.length != 3) throw new ArrayIndexOutOfBoundsException("Invalid unit format");
                    else {
                        army.addUnit(unitFactory.createUnit(unitType, name, health));
                    }
                }
                return army;
            }
        }
    }

        /**
         * Deprecated method.
         * Method for writing information about an army to a csv file
         * Takes in an army, and reads it to a CSV file.
         * Not utilized anywhere in the program code.
         *
         * @param
         */
        public void armyToCsv (String filePath, Army army){
            try {
                FileWriter outputfile = new FileWriter(filePath);
                CSVWriter writer = new CSVWriter(outputfile);

                for (Unit unit : army.getAllUnits()) {
                    String[] unitData = {unit.getClass().getSimpleName(), unit.getName(), String.valueOf(unit.getHealth())};
                    writer.writeNext(unitData);
                }

                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    /**
     * Opens the computers file explorer.
     * Path of the file that is selected in the file explorer is returned as a String.
     * @return String - filepath of the selected file.
     */
    public String getFilePath () {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("src/main/resources/SampleArmies"));
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files", "*.csv"));
            File selectedFile = fileChooser.showOpenDialog(WargamesApp.getStage());
            return selectedFile.getPath();
        }
    }
