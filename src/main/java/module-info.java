module wargames {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.opencsv;

    exports Game.GUI.View;
    opens Game.GUI.View;
    opens Game.GUI.Controllers;
    exports Game.GUI.Controllers to javafx.fxml;
    opens Game.War.Units.SpecialUnits to org.junit.jupiter;

}